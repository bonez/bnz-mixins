gulp           = require 'gulp'
jade           = require 'gulp-jade'
stylus         = require 'gulp-stylus'
uglify         = require 'gulp-uglify'
concat         = require 'gulp-concat'
notify         = require 'gulp-notify'
coffee         = require 'gulp-coffee'
rename         = require 'gulp-rename'
replace        = require 'gulp-replace'
clean          = require 'gulp-clean'
cache          = require 'gulp-cache'
bower          = require 'gulp-bower'
colors         = require 'colors'
mainBowerFiles = require 'main-bower-files'
BNZcommonJS    = require 'bnz-commonjs'
rupture        = require 'rupture'


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rootPath   = '.'
assetsPath = "#{rootPath}/assets"
publicPath = "#{rootPath}/public"

paths = do ->

    paths =

        coffee:         [['coffee/app/**/*.coffee', 'coffee/init.coffee'], 'js']
#        html:           ['jade/*.jade']
        templates:      ['jade/templates/*.jade', 'js']
#        stylus:         ['stylus/**/*.styl', 'css']

    for name, arr of paths
        if name isnt 'copyFonts'
            if typeof arr[0] is 'object' then for a, i in arr[0]
                arr[0][i] = if /^!/.test a then "!#{assetsPath}/#{a.substring 1}" else "#{assetsPath}/#{a}"
            else arr[0] = "#{assetsPath}/#{arr[0]}"
        arr[1] = "#{publicPath}/#{if arr[1]? then "#{arr[1]}/" else ''}"

    paths


commonTasks = (path for path of paths when not /^copy/.test path)

copyTasks = (lowercaseFirstLetter path.substr 4 for path of paths when /^copy/.test path)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

ntf = (title) -> notify.onError (error) -> "\n\n::: #{title} :::\n\n#{error.message}\n\n"

lowercaseFirstLetter = (string) -> string.charAt(0).toLowerCase() + string.slice 1

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


gulp.task 'create-js-index', ->

    files = [
        "#{publicPath}/js/app.js"
        "#{publicPath}/js/templates.js"
    ]

    gulp.src files
        .pipe concat 'index.js'
        .pipe gulp.dest rootPath



gulp.task 'html', ->
    gulp.src paths.html[0]
        .pipe jade pretty: true
        .on 'error', -> @emit 'end'
        .on 'error', ntf 'JADE HTML'
        .pipe gulp.dest paths.html[1]


gulp.task 'coffee', ->
    gulp.src "#{assetsPath}/coffee/app/*.coffee"
        .pipe cache coffee bare: true
        .pipe concat 'app.js'
        .pipe replace /var __indexOf = .*return -1; \};\n\n/img, ''
        .pipe replace "});\n", '});'
        .pipe gulp.dest "#{publicPath}/js"

gulp.task 'templates', ->
    gulp.src paths.templates[0]
        .pipe jade client: true
        .on 'error', -> @emit 'end'
        .on 'error', ntf 'JADE TEMPLATES'
        .pipe BNZcommonJS.wrapTemplate()
        .pipe concat 'templates.js'
        .pipe gulp.dest paths.templates[1]


gulp.task 'bower', ->
    bower()
        .on 'end', ->
            gulp.src mainBowerFiles()
                .pipe concat 'vendors.js'
                .pipe gulp.dest "#{publicPath}/js/"
        .pipe gulp.dest 'dependencies'


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

gulp.task 'watch', -> for task in commonTasks then gulp.watch paths[task][0], [task]

gulp.task 'default', commonTasks.concat ['bower', 'watch']
