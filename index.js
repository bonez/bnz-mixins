define('AbstractBlockShadowsView', function(module) {
  'use strict';
  var AbstractBlockShadowsView, bottomMargin, decoratorClassName, initShadow, isArray, pint, render, step;
  pint = function(n) {
    return Math.abs(parseInt(n, 10));
  };
  bottomMargin = 100;
  decoratorClassName = 'shadow-decorator';
  step = bottomMargin / 10;
  isArray = function(arr) {
    return '[object NodeList]' === Object.prototype.toString.call(arr);
  };
  render = function(el) {
    el.classList.add(decoratorClassName);
    return el.insertAdjacentHTML('BeforeEnd', '<div class="ts"><div></div></div><div class="bs"><div></div></div>');
  };
  initShadow = function(el, renderBottom) {
    var bs, ct, ts;
    render.call(this, el);
    ct = el.querySelector('.ct');
    ts = el.querySelector('.ts > div');
    bs = el.querySelector('.bs > div');
    return ct.addEventListener('scroll', function() {
      var ctBottomMargin, ctScrollHeight, o;
      ctScrollHeight = ct.scrollHeight - ct.offsetHeight;
      ts.style.opacity = ct.scrollTop >= step ? 1 : "0." + ct.scrollTop;
      if (renderBottom) {
        ctBottomMargin = ctScrollHeight - bottomMargin;
        if (ct.scrollTop < ctBottomMargin) {
          bs.style.opacity = 1;
        } else {
          o = bottomMargin / step - pint((ctBottomMargin - ct.scrollTop) / step);
          if (o < step) {
            bs.style.opacity = "0." + o;
          } else {
            1;
          }
        }
      }
      if (ct.scrollTop === 0) {
        return bs.style.opacity = 0;
      }
    }, false);
  };
  module.exports = AbstractBlockShadowsView = (function() {
    function AbstractBlockShadowsView() {}

    AbstractBlockShadowsView.prototype.initShadows = function(els, renderBottom) {
      var el, _i, _len;
      if (renderBottom == null) {
        renderBottom = false;
      }
      if (isArray(els)) {
        for (_i = 0, _len = els.length; _i < _len; _i++) {
          el = els[_i];
          initShadow.call(this, el, renderBottom);
        }
      } else {
        initShadow.call(this, els, renderBottom);
      }
    };

    return AbstractBlockShadowsView;

  })();
});
define('AbstractDragAndDropView', function(module, require) {
  'use strict';
  var AbstractDragAndDropView, EE, Helper, classes, currentlyDragged, disableDragAndDropElements, elementDragged, enableDragAndDropElements, html, ico, isAllowed, prevent, stopDragging;
  elementDragged = null;
  Helper = require('Helper');
  EE = Helper.EE;
  ico = Helper.icon;
  prevent = function(e, hasParent) {
    var item;
    if (e.preventDefault) {
      e.preventDefault();
    }
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    item = hasParent ? e.currentTarget.parentNode : e.currentTarget;
    return item !== elementDragged;
  };
  stopDragging = function() {
    this.el.removeAttribute('draggable');
    disableDragAndDropElements.call(this);
    return elementDragged = null;
  };
  classes = {
    dragAndDropBody: 'drag-and-drop',
    over: 'over',
    blocked: 'blocked',
    handler: 'drag-handler'
  };
  html = document.querySelector('html');
  currentlyDragged = null;
  enableDragAndDropElements = function() {
    currentlyDragged = this.el.data('type');
    return html.addClass("" + classes.dragAndDropBody + " drag-" + currentlyDragged);
  };
  disableDragAndDropElements = function() {
    currentlyDragged = null;
    return html.removeClass("" + classes.dragAndDropBody + " drag-" + (this.el.data('type')));
  };
  isAllowed = function() {
    return ((this.el.data("allow-drop-" + currentlyDragged)) != null) && !this.el.hasClass(classes.blocked);
  };
  module.exports = AbstractDragAndDropView = (function() {
    function AbstractDragAndDropView() {}


    /*
        @param {array} [order|unit] allowWhat - describe what what block are allowed to drop there
        @param {function} callback - on drop callback
     */

    AbstractDragAndDropView.prototype.initDrop = function(allowWhat, callback, dropAreaClassName) {
      var what, _i, _len;
      if (allowWhat == null) {
        allowWhat = [];
      }
      if (dropAreaClassName == null) {
        dropAreaClassName = 'drop-area';
      }
      for (_i = 0, _len = allowWhat.length; _i < _len; _i++) {
        what = allowWhat[_i];
        this.el.data("allow-drop-" + what, '');
      }
      if (this.el.querySelector("." + dropAreaClassName) == null) {
        this.el.appendChild(EE('div', dropAreaClassName));
      }
      this.events["dragenter ." + dropAreaClassName] = (function(_this) {
        return function(e) {
          if (prevent(e, true)) {
            _this.el.addClass(classes.over);
          }
          return false;
        };
      })(this);
      this.events["dragleave ." + dropAreaClassName] = (function(_this) {
        return function(e) {
          if (prevent(e, true)) {
            _this.el.removeClass(classes.over);
          }
          return false;
        };
      })(this);
      _.extend(this.events, {
        'dragover': function(e) {
          prevent(e);
          e.originalEvent.dataTransfer.dropEffect = 'move';
          return false;
        },
        'drop': (function(_this) {
          return function(e) {
            if (prevent(e)) {
              _this.el.removeClass(classes.over);
              if (isAllowed.call(_this)) {
                if (typeof callback === "function") {
                  callback(elementDragged);
                }
              }
            }
            return false;
          };
        })(this)
      });      this.delegateEvents();
    };

    AbstractDragAndDropView.prototype.initDrag = function() {
      if (!this.el.querySelector("." + (ico('move')) + "." + classes.handler)) {
        this.el.appendChild(EE('div', "" + (ico('move')) + " " + classes.handler));
      }
      this.events["mousedown ." + classes.handler] = (function(_this) {
        return function() {
          _this.el.setAttribute('draggable', true);
          return enableDragAndDropElements.call(_this);
        };
      })(this);
      this.events["mouseup ." + classes.handler] = _.bind(stopDragging, this);
      _.extend(this.events, {
        'dragstart': (function(_this) {
          return function(e) {
            e.originalEvent.dataTransfer.effectAllowed = 'move';
            e.originalEvent.dataTransfer.setData('text', _this.innerHTML);
            return elementDragged = _this.el;
          };
        })(this),
        'dragend': _.bind(stopDragging, this)
      });      this.delegateEvents();
    };

    return AbstractDragAndDropView;

  })();
});
define('mixins-forms', function(module, require) {
  var EE, FormCheckboxTpl, FormFieldTpl, Helper, trans;
  FormCheckboxTpl = require('FormCheckboxTpl');
  FormFieldTpl = require('FormFieldTpl');
  Helper = require('Helper');
  trans = Helper.trans;
  EE = Helper.EE;
  return (function(Form) {
    'use strict';
    var mergeWhat, tplFormCheckbox;
    Form.Field.prototype.templateData = function() {
      var required, result, schema;
      schema = this.schema;
      required = (schema.validators != null) && __indexOf.call(schema.validators, 'required') >= 0;
      result = {
        title: schema.title,
        fieldAttrs: schema.fieldAttrs,
        editorAttrs: schema.editorAttrs,
        key: this.key,
        editorId: this.editor.id,
        inPlaceAdd: !!schema.inPlaceAdd,
        required: required,
        editorExtraClass: schema.editorExtraClass,
        hidden: !!schema.hiddenInForm
      };
      if (!!schema.checkbox) {
        if (!!schema.checkbox.switcher) {
          result.switcher = true;
          if (!!schema.checkbox.rounded) {
            result.rounded = true;
          }
          if (!!schema.checkbox.values) {
            result.on = schema.checkbox.values.on;
            result.off = schema.checkbox.values.off;
          }
        }
      }
      return result;
    };
    Form.Field.prototype.createSchema = function(schema) {
      var defaults;
      if (_.isString(schema)) {
        schema = {
          type: schema
        };
      }
      defaults = {
        type: 'Text',
        title: this.createTitle()
      };
      schema = _.extend(defaults, schema);
      if (_.isString(schema.type)) {
        schema._type = schema.type;
        schema.type = Form.editors[schema.type];
      }
      return schema;
    };
    Form.Field.template = FormFieldTpl;
    mergeWhat = null;
    Form.Fieldset.prototype.render = function() {
      var $fieldset, fields;
      fields = this.fields;
      $fieldset = $($.trim(this.template(_.result(this, 'templateData'))));
      this.setElement($fieldset);
      $fieldset.find('[data-fields]').add($fieldset).each((function(_this) {
        return function(i, el) {
          var $container, $div, $el, $mergeWhat, field, firstEditor, id, secondEditor, selection, separator, _results;
          $container = $(el);
          selection = $container.attr('data-fields');
          if (_.isUndefined(selection)) {
            return;
          }
          _results = [];
          for (id in fields) {
            field = fields[id];
            el = field.render().el;
            if (el != null) {
              if (!!field.schema.mergeEditorWithNext) {
                _results.push(mergeWhat = field);
              } else {
                if (mergeWhat != null) {
                  $mergeWhat = mergeWhat.render().$el;
                  $el = field.render().$el;
                  $div = $(EE('div'));
                  $container.append($div);
                  $div.append($(EE('div', 'titles')).append($mergeWhat.find('> label'), $el.find('> label')));
                  firstEditor = $mergeWhat.find('> div');
                  secondEditor = $el.find('> div');
                  mergeWhat.setElement(firstEditor);
                  field.setElement(secondEditor);
                  separator = EE('div', 'separator', {
                    text: '/'
                  });                  $div.append($(EE('div', 'editors')).append(firstEditor, separator, secondEditor));
                  _results.push(mergeWhat = null);
                } else {
                  _results.push($container.append(el));
                }
              }
            } else {
              _results.push(void 0);
            }
          }
          return _results;
        };
      })(this));
      return this;
    };
    Form.editors.Checkbox.prototype.getValue = function() {
      return this.$('[type="checkbox"]').prop('checked');
    };
    Form.editors.Checkbox.prototype.setValue = function(value) {
      return this.$('[type="checkbox"]').prop('checked', value ? true : false);
    };
    tplFormCheckbox = FormCheckboxTpl;
    Form.editors.Checkbox.prototype.render = function() {
      var params, _ref, _ref1;
      params = {
        switcher: true
      };
      if (((_ref = this.schema.editorAttrs) != null ? _ref.disabled : void 0) != null) {
        params['disabled'] = (_ref1 = this.schema.editorAttrs) != null ? _ref1.disabled : void 0;
      }
      this.setElement(tplFormCheckbox(params));
      this.setValue(this.value);
      return this;
    };
    Form.validators.errMessages.required = trans('forms.errors.required');
    (function() {
      var i, maxVals, message, _i, _len, _results;
      maxVals = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
      message = trans('forms.errors.max_length');
      _results = [];
      for (_i = 0, _len = maxVals.length; _i < _len; _i++) {
        i = maxVals[_i];
        _results.push((function(i) {
          var j;
          j = i;
          return Form.validators["max" + i] = function() {
            return function(v) {
              if (v.length > j) {
                return {
                  message: "" + message + " " + j
                };
              }
            };
          };
        })(i));
      }
      return _results;
    })();
    (function() {
      var i, message, minVals, _i, _len, _results;
      minVals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
      message = trans('forms.errors.min_length');
      _results = [];
      for (_i = 0, _len = minVals.length; _i < _len; _i++) {
        i = minVals[_i];
        _results.push((function(i) {
          var j;
          j = i;
          return Form.validators["min" + i] = function() {
            return function(v) {
              if (v.length < j) {
                return {
                  message: "" + message + " " + j
                };
              }
            };
          };
        })(i));
      }
      return _results;
    })();
    _.extend(Form.validators, {
      float: function() {
        return Form.validators.regexp({
          message: trans('forms.errors.invalid_numbers_only'),
          regexp: /^[+-]?\d+(\,\d+)?$/
        });      },
      integer: function() {
        return Form.validators.regexp({
          message: trans('forms.errors.invalid_integer_only'),
          regexp: /^\d+$/
        });      },
      date: function() {
        return Form.validators.regexp({
          message: trans('forms.errors.invalid_date_format'),
          regexp: /^\d{4}-\d{1,2}-\d{1,2}$/
        });      },
      email: function() {
        return Form.validators.regexp({
          message: trans('forms.errors.invalid_email'),
          regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        });      }
    });  })(Backbone.Form);
});
(function() {
  'use strict';
  Array.prototype.insert = function(index, value) {
    var arg, _i, _len, _ref;
    _ref = _.rest(arguments);
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      arg = _ref[_i];
      this.splice(index++, 0, arg);
    }
    return this;
  };
  Array.prototype.reIndexOf = function(rx) {
    var i, item, _i, _len;
    for (i = _i = 0, _len = this.length; _i < _len; i = ++_i) {
      item = this[i];
      if (item.toString().match(rx)) {
        return i;
      }
    }
    return -1;
  };
  String.prototype.capitalize = function() {
    return "" + (this.charAt(0).toUpperCase()) + (this.slice(1));
  };
  String.prototype.toDOMEl = function() {
    var tmp;
    tmp = document.createElement('div');
    tmp.innerHTML = this;
    if ((tmp.firstChild == null) || tmp.firstChild.toString() === '[object Text]') {
      throw Error("\"" + this + "\" is not valid string!");
    }
    return tmp.firstChild;
  };
  HTMLElement.prototype.type = function(value) {
    if (!value) {
      return this.getAttribute('data-type');
    } else {
      this.setAttribute('data-type', value);
    }
    return value;
  };
  HTMLElement.prototype.data = function(name, value) {
    if (value != null) {
      this.setAttribute("data-" + name, value);
      return this;
    } else {
      return this.getAttribute("data-" + name);
    }
  };
  HTMLElement.prototype.addClass = function(className) {
    (className.split(' ') || []).forEach(function(itemClass) {
      if (itemClass) {
        return this.classList.add(itemClass);
      }
    }, this);
    return this;
  };
  HTMLElement.prototype.removeClass = function(className) {
    (className.split(' ') || []).forEach(function(itemClass) {
      if (itemClass) {
        return this.classList.remove(itemClass);
      }
    }, this);
    return this;
  };
  HTMLElement.prototype.toggleClass = function(className) {
    var parts;
    parts = className.split(' ');
    if (parts.length > 1) {
      this.classList.toggle(parts[0]);
      this.classList.toggle(parts[1]);
    } else {
      this.classList.toggle(className);
    }
    return this;
  };
  HTMLElement.prototype.hasClass = function(className) {
    return this.classList.contains(className);
  };
  HTMLElement.prototype.hide = function() {
    this.style.display = 'none';
    return this;
  };
  HTMLElement.prototype.show = function() {
    this.style.display = 'block';
    return this;
  };
  HTMLElement.prototype.toggle = function() {
    if (!!this.style.display && this.style.display === 'none') {
      this.style.display = 'block';
    } else {
      this.style.display = 'none';
    }
    return this;
  };
  HTMLElement.prototype.index = function() {
    var arr;
    arr = Array.prototype.slice.call(this.parentNode.children);
    return arr.indexOf(this);
  };
  HTMLElement.prototype.append = function(el) {
    switch (typeof el) {
      case 'string':
        this.insertAdjacentHTML('BeforeEnd', el);
        break;
      case 'object':
        if (/\[object HTML.*Element\]/g.test(el.toString())) {
          this.appendChild(el);
        }
    }
    return this;
  };
  HTMLElement.prototype.before = function(el) {
    var div, parent;
    switch (typeof el) {
      case 'string':
        div = document.createElement('div');
        div.innerHTML = el;
        el = div.firstChild;
        break;
      case 'object':
        if (!/\[object HTML.*Element\]/g.test(el.toString())) {
          throw Error("\"" + el + "\" is not valid!");
        }
    }
    parent = this.parentNode;
    parent.insertBefore(el, this);
    return el;
  };
  HTMLElement.prototype.empty = function() {
    while (this.firstChild) {
      this.removeChild(this.firstChild);
    }
    return this;
  };
  HTMLElement.prototype.remove = function() {
    this.parentNode.removeChild(this);
    return this;
  };
  HTMLElement.prototype.html = function(html) {
    switch (typeof html) {
      case 'string':
        this.innerHTML = html;
        break;
      case 'object':
        if (/\[object HTML.*Element\]/g.test(html.toString())) {
          this.empty();
          this.appendChild(html);
        }
    }
    return this;
  };
  NodeList.prototype.show = function() {
    Array.prototype.forEach.call(this, function(item) {
      return item.show();
    });    return this;
  };
  NodeList.prototype.hide = function() {
    Array.prototype.forEach.call(this, function(item) {
      return item.hide();
    });    return this;
  };
  NodeList.prototype.remove = function() {
    Array.prototype.forEach.call(this, function(item) {
      return item.remove();
    });    return this;
  };
  NodeList.prototype.addClass = function(className) {
    Array.prototype.forEach.call(this, function(item) {
      return item.addClass(className);
    });    return this;
  };
  NodeList.prototype.removeClass = function(className) {
    Array.prototype.forEach.call(this, function(item) {
      if (className) {
        return item.removeClass(className);
      } else {
        return item.removeAttribute('class');
      }
    });    return this;
  };
  NodeList.prototype.toggleClass = function(className) {
    var parts;
    parts = className.split(' ');
    Array.prototype.forEach.call(this, function(item) {
      if (parts.length > 1) {
        item.classList.toggle(parts[0]);
        return item.classList.toggle(parts[1]);
      } else {
        return item.classList.toggle(className);
      }
    });    return this;
  };
  Backbone.Collection.prototype.filterValues = function(filterValue) {
    if (filterValue === '') {
      return [];
    }
    return this.filter(function(model) {
      var data, key, value;
      data = model.toJSON();
      delete data['_asRef'];
      for (key in data) {
        value = data[key];
        if ((value != null) && (value['dn'] != null)) {
          data[key] = value['dn'];
        }
      }
      return _.some(_.values(data), function(value) {
        if (_.isNumber(value)) {
          value = value.toString();
        }
        if (typeof value === 'object' && (value != null)) {
          value = value.label || value.date || '';
        }
        if (_.isString(value)) {
          value = value.toLowerCase();
          return -1 !== value.indexOf(filterValue);
        }
        return false;
      });    });  };
})();

define('FormCheckboxTpl', function (module, require) {
module.exports = function (locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (require, switcher, rounded, editorId, disabled, withEditor, Math, titleBefore, title) {

var className = 'checkbox-option',
trans = require('Helper').trans,
on = 'I',
off = 'O';
if (switcher) className = 'checkbox-switcher';
if (rounded) {
className += ' rounded';
on = trans('forms.checkbox.on');
off = trans('forms.checkbox.off');
}
jade_mixins["checkboxInput"] = function(){
var block = (this && this.block), attributes = (this && this.attributes) || {};
var attrs = { type: 'checkbox' };
attrs.id = editorId;
attrs.name = name;
if (disabled) attrs.disabled = 'disabled';
buf.push("<label data-editor" + (jade.cls([className], [true])) + "><input" + (jade.attrs(jade.merge([attrs]), true)) + "><span" + (jade.attr("data-on", on, true, true)) + (jade.attr("data-off", off, true, true)) + "></span></label>");
};
if ( (withEditor))
{
if (!editorId) editorId = 'input_' + Math.floor(Math.random() * (1000 - 1) + 1);
if (!name) var name = editorId;
jade_mixins["checkboxInput"]();
}
else
{
buf.push("<div>");
if ( (titleBefore))
{
if ( (title))
{
buf.push("<label" + (jade.attr("for", editorId, true, true)) + ">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</label>");
}
jade_mixins["checkboxInput"]();
buf.push("<div data-error class=\"bbf-error\"></div>");
}
else
{
jade_mixins["checkboxInput"]();
buf.push("<div data-error class=\"bbf-error\"></div>");
if ( (title))
{
buf.push("<label" + (jade.attr("for", editorId, true, true)) + ">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</label>");
}
}
buf.push("</div>");
}}.call(this,"require" in locals_for_with?locals_for_with.require:typeof require!=="undefined"?require:undefined,"switcher" in locals_for_with?locals_for_with.switcher:typeof switcher!=="undefined"?switcher:undefined,"rounded" in locals_for_with?locals_for_with.rounded:typeof rounded!=="undefined"?rounded:undefined,"editorId" in locals_for_with?locals_for_with.editorId:typeof editorId!=="undefined"?editorId:undefined,"disabled" in locals_for_with?locals_for_with.disabled:typeof disabled!=="undefined"?disabled:undefined,"withEditor" in locals_for_with?locals_for_with.withEditor:typeof withEditor!=="undefined"?withEditor:undefined,"Math" in locals_for_with?locals_for_with.Math:typeof Math!=="undefined"?Math:undefined,"titleBefore" in locals_for_with?locals_for_with.titleBefore:typeof titleBefore!=="undefined"?titleBefore:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
}
});
define('FormFieldTpl', function (module, require) {
module.exports = function (locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (hidden, editorExtraClass, editorId, title, required) {

if ( !hidden)
{
buf.push("<div" + (jade.cls([editorExtraClass], [true])) + "><label" + (jade.attr("for", editorId, true, true)) + ">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)));
if ( (required))
{
buf.push("<em>*</em>");
}
buf.push("</label><div><div data-editor></div><div data-error></div></div></div>");
}}.call(this,"hidden" in locals_for_with?locals_for_with.hidden:typeof hidden!=="undefined"?hidden:undefined,"editorExtraClass" in locals_for_with?locals_for_with.editorExtraClass:typeof editorExtraClass!=="undefined"?editorExtraClass:undefined,"editorId" in locals_for_with?locals_for_with.editorId:typeof editorId!=="undefined"?editorId:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined,"required" in locals_for_with?locals_for_with.required:typeof required!=="undefined"?required:undefined));;return buf.join("");
}
});