# bnz-mixins
# 2014-10-15 11:57
# bonez

do ->
    'use strict'

    Array::insert = (index, value) ->
        for arg in _.rest arguments then @splice index++, 0, arg
        @

    Array::reIndexOf = (rx) ->
        for item, i in @ when item.toString().match rx then return i
        -1

    String::capitalize = -> "#{@charAt(0).toUpperCase()}#{@slice 1}"

    String::toDOMEl = ->
        tmp = document.createElement 'div'
        tmp.innerHTML = @
        if not tmp.firstChild? or tmp.firstChild.toString() is '[object Text]'
            throw Error "\"#{@}\" is not valid string!"
        tmp.firstChild

    HTMLElement::type = (value) ->
        if not value then return @getAttribute 'data-type'
        else @setAttribute 'data-type', value
        value

    HTMLElement::data = (name, value) ->
        if value?
            @setAttribute "data-#{name}", value
            @
        else
            @getAttribute "data-#{name}"

    HTMLElement::addClass = (className) ->
        (className.split(' ') || []).forEach (itemClass) ->
            if itemClass then @classList.add itemClass
        , @; @

    HTMLElement::removeClass = (className) ->
        (className.split(' ') || []).forEach (itemClass) ->
            if itemClass then @classList.remove itemClass
        , @; @

    HTMLElement::toggleClass = (className) ->
        parts = className.split ' '
        if parts.length > 1
            @classList.toggle parts[0]
            @classList.toggle parts[1]
        else @classList.toggle className
        @

    HTMLElement::hasClass = (className) -> @classList.contains className

    HTMLElement::hide = ->
        @style.display = 'none'
        @

    HTMLElement::show = ->
        @style.display = 'block'
        @

    HTMLElement::toggle = ->
        if !!@style.display and @style.display is 'none'
            @style.display = 'block'
        else @style.display = 'none'
        @

    HTMLElement::index = ->
        arr = Array.prototype.slice.call @parentNode.children
        arr.indexOf @

    HTMLElement::append = (el) ->
        switch typeof el
            when 'string' then @insertAdjacentHTML 'BeforeEnd', el
            when 'object'
                if /\[object HTML.*Element\]/g.test el.toString()
                    @appendChild el
        @

    HTMLElement::before = (el) ->
        switch typeof el
            when 'string'
                div = document.createElement 'div'
                div.innerHTML = el
                el = div.firstChild
            when 'object'
                if not /\[object HTML.*Element\]/g.test el.toString()
                    throw Error "\"#{el}\" is not valid!"
        parent = @parentNode
        parent.insertBefore el, @
        el

    HTMLElement::empty = ->
        while @firstChild then @removeChild @firstChild
        @

    HTMLElement::remove = ->
        @parentNode.removeChild @
        @

    HTMLElement::html = (html) ->
        switch typeof html
            when 'string' then @innerHTML = html
            when 'object'
                if /\[object HTML.*Element\]/g.test html.toString()
                    @empty()
                    @appendChild html
        @


    NodeList::show = ->
        Array::forEach.call @, (item) -> item.show()
        @

    NodeList::hide = ->
        Array::forEach.call @, (item) -> item.hide()
        @

    NodeList::remove = ->
        Array::forEach.call @, (item) -> item.remove()
        @

    NodeList::addClass = (className) ->
        Array::forEach.call @, (item) -> item.addClass className
        @

    NodeList::removeClass = (className) ->
        Array::forEach.call @, (item) ->
            if className then item.removeClass className
            else item.removeAttribute 'class'
        @

    NodeList::toggleClass = (className) ->
        parts = className.split ' '
        Array::forEach.call @, (item) ->
            if parts.length > 1
                item.classList.toggle parts[0]
                item.classList.toggle parts[1]
            else item.classList.toggle className
        @

    Backbone.Collection::filterValues = (filterValue) ->
        if filterValue is '' then return []
        @filter (model) ->
            data = model.toJSON()
            delete data['_asRef']
            for key, value of data when value? and value['dn']?
                data[key] = value['dn']
            _.some (_.values data), (value) ->
                if _.isNumber value then value = value.toString()
                if typeof value is 'object' and value?
                    value = value.label or value.date or ''
                if _.isString value
                    value = value.toLowerCase()
                    return -1 isnt value.indexOf filterValue
                return false

    return
