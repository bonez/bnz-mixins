# bnz-mixins
# 2014-10-15 18:25
# bonez

define 'AbstractDragAndDropView', (module, require) ->
    'use strict'

    #
    # Private
    #

    elementDragged = null

    Helper = require 'Helper'
    EE = Helper.EE
    ico = Helper.icon

    prevent = (e, hasParent) ->
        do e.preventDefault if e.preventDefault
        do e.stopPropagation if e.stopPropagation
        item = if hasParent then e.currentTarget.parentNode else e.currentTarget
        item isnt elementDragged

    stopDragging = ->
        @el.removeAttribute 'draggable'
        disableDragAndDropElements.call @
        elementDragged = null

    classes =
        dragAndDropBody: 'drag-and-drop'
        over: 'over'
        blocked: 'blocked'
        handler: 'drag-handler'

    html = document.querySelector 'html'

    currentlyDragged = null

    enableDragAndDropElements = ->
        currentlyDragged = @el.data 'type'
        html.addClass "#{classes.dragAndDropBody} drag-#{currentlyDragged}"

    disableDragAndDropElements = ->
        currentlyDragged = null
        html.removeClass "#{classes.dragAndDropBody} drag-#{@el.data 'type'}"

    # TODO
    # check if in kit, is blocked and etc.
    isAllowed = ->
        (@el.data "allow-drop-#{currentlyDragged}")? and not @el.hasClass classes.blocked


    module.exports = class AbstractDragAndDropView

        #
        # Public
        #

        ###
            @param {array} [order|unit] allowWhat - describe what what block are allowed to drop there
            @param {function} callback - on drop callback
        ###
        initDrop: (allowWhat = [], callback, dropAreaClassName = 'drop-area') ->

            for what in allowWhat then @el.data "allow-drop-#{what}", ''

            if not @el.querySelector(".#{dropAreaClassName}")?
                @el.appendChild EE 'div', dropAreaClassName

            @events["dragenter .#{dropAreaClassName}"] = (e) =>
                if prevent e, true then @el.addClass classes.over
                false

            @events["dragleave .#{dropAreaClassName}"] = (e) =>
                if prevent e, true then @el.removeClass classes.over
                false

            _.extend @events,

                'dragover': (e) ->
                    prevent e
                    e.originalEvent.dataTransfer.dropEffect = 'move'
                    false

                'drop': (e) =>
                    if prevent e
                        @el.removeClass classes.over
                        if isAllowed.call @ then callback? elementDragged
                    false

            do @delegateEvents

            return

        initDrag: ->

            if not @el.querySelector ".#{ico 'move'}.#{classes.handler}"
                @el.appendChild EE 'div', "#{ico 'move'} #{classes.handler}"

            @events["mousedown .#{classes.handler}"] = =>
                @el.setAttribute 'draggable', true
                enableDragAndDropElements.call @

            @events["mouseup .#{classes.handler}"] = _.bind stopDragging, @

            _.extend @events,

                'dragstart': (e) =>
                    e.originalEvent.dataTransfer.effectAllowed = 'move'
                    e.originalEvent.dataTransfer.setData 'text', @innerHTML
                    elementDragged = @el

                'dragend': _.bind stopDragging, @

            do @delegateEvents

            return

    return
