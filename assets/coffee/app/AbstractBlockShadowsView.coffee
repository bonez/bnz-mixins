# bnz-mixins
# 2014-10-15 13:55
# bonez
define 'AbstractBlockShadowsView', (module) ->
    'use strict'

    pint = (n) -> Math.abs parseInt n, 10

    bottomMargin = 100

    decoratorClassName = 'shadow-decorator'

    step = bottomMargin / 10

    isArray = (arr) -> '[object NodeList]' is Object::toString.call arr

    render = (el) ->
        el.classList.add decoratorClassName
        # el.insertAdjacentHTML 'AfterBegin', ''
        el.insertAdjacentHTML 'BeforeEnd', '<div class="ts"><div></div></div><div class="bs"><div></div></div>'


    initShadow = (el, renderBottom) ->

        render.call @, el

        ct = el.querySelector '.ct'
        ts = el.querySelector '.ts > div'
        bs = el.querySelector '.bs > div'

        ct.addEventListener 'scroll', ->
            ctScrollHeight = ct.scrollHeight - ct.offsetHeight
            ts.style.opacity = if ct.scrollTop >= step then 1 else "0.#{ct.scrollTop}"

            if renderBottom
                ctBottomMargin = ctScrollHeight - bottomMargin
                if ct.scrollTop < ctBottomMargin then bs.style.opacity = 1 else
                    o = bottomMargin / step - pint (ctBottomMargin - ct.scrollTop) / step
                    if o < step then bs.style.opacity = "0.#{o}" else 1

            if ct.scrollTop is 0 then bs.style.opacity = 0

        , false

    module.exports = class AbstractBlockShadowsView

        initShadows: (els, renderBottom = false) ->
            if isArray els then for el in els then initShadow.call @, el, renderBottom
            else initShadow.call @, els, renderBottom
            return

    return
