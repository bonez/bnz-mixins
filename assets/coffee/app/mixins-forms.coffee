# bnz-mixins
# 2014-10-15 12:08
# bonez

define 'mixins-forms', (module, require) ->

    FormCheckboxTpl = require 'FormCheckboxTpl'
    FormFieldTpl = require 'FormFieldTpl'
    Helper = require 'Helper'

    trans = Helper.trans
    EE = Helper.EE

    do (Form = Backbone.Form) ->
        'use strict'

        # - Templates - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        Form.Field::templateData = ->
            schema = @schema
            required = schema.validators? and 'required' in schema.validators

            result =
                title: schema.title
                fieldAttrs: schema.fieldAttrs
                editorAttrs: schema.editorAttrs
                key: @key
                editorId: @editor.id
                inPlaceAdd: !!schema.inPlaceAdd
                required: required
                editorExtraClass: schema.editorExtraClass
                hidden: !!schema.hiddenInForm

            if not !schema.checkbox
                if !!schema.checkbox.switcher
                    result.switcher = true
                    if !!schema.checkbox.rounded then result.rounded = true
                    if !!schema.checkbox.values
                        result.on = schema.checkbox.values.on
                        result.off = schema.checkbox.values.off
            result


        Form.Field::createSchema = (schema) ->
            if _.isString schema then schema = type: schema
            defaults = type: 'Text', title: @createTitle()
            schema = _.extend defaults, schema
            if _.isString schema.type
                schema._type = schema.type
                schema.type = Form.editors[schema.type]
            schema


        Form.Field.template = FormFieldTpl

        mergeWhat = null

        Form.Fieldset::render = ->
            fields = @fields
            $fieldset = $ $.trim @template _.result @, 'templateData'
            @setElement $fieldset

            $fieldset
                .find '[data-fields]'
                .add $fieldset
                .each (i, el) =>
                    $container = $ el
                    selection = $container.attr 'data-fields'
                    if _.isUndefined selection then return

                    for id, field of fields
                        el = field.render().el
                        if el?

                            if not not field.schema.mergeEditorWithNext
                                mergeWhat = field
                            else

                                if mergeWhat?
                                    $mergeWhat = mergeWhat.render().$el
                                    $el = field.render().$el
                                    $div = $ EE 'div'
                                    $container.append $div
                                    $div.append $(EE 'div', 'titles').append $mergeWhat.find('> label'), $el.find '> label'
                                    firstEditor = $mergeWhat.find '> div'
                                    secondEditor = $el.find '> div'
                                    mergeWhat.setElement firstEditor
                                    field.setElement secondEditor
                                    separator = EE 'div', 'separator', text: '/'
                                    $div.append $(EE 'div', 'editors').append firstEditor, separator, secondEditor
                                    mergeWhat = null
                                else
                                    $container.append el

            @

        Form.editors.Checkbox::getValue = ->
            @$ '[type="checkbox"]'
                .prop 'checked'

        Form.editors.Checkbox::setValue = (value) ->
            @$ '[type="checkbox"]'
                .prop 'checked', if value then true else false

        tplFormCheckbox = FormCheckboxTpl

        Form.editors.Checkbox::render = ->
            params = switcher: true
            if @schema.editorAttrs?.disabled?
                params['disabled'] = @schema.editorAttrs?.disabled
            @setElement tplFormCheckbox params
            @setValue @value
            @


        # - Validators - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


        # do (V = Form.validators) =>
        Form.validators.errMessages.required = trans 'forms.errors.required'

        # max length
        do ->
            maxVals = [2..20]
            message = trans 'forms.errors.max_length'
            (do (i) -> j = i; Form.validators["max#{i}"] = -> (v) -> if v.length > j then message: "#{message} #{j}") for i in maxVals


        do ->
            minVals = [1..15]
            message = trans 'forms.errors.min_length'
            (do (i) -> j = i; Form.validators["min#{i}"] = -> (v) -> if v.length < j then message: "#{message} #{j}") for i in minVals


        _.extend Form.validators,

            float: ->
                Form.validators.regexp
                    message: trans 'forms.errors.invalid_numbers_only'
                    regexp: /^[+-]?\d+(\,\d+)?$/

            integer: ->
                Form.validators.regexp
                    message: trans 'forms.errors.invalid_integer_only'
                    regexp: /^\d+$/

            date: -> # yyyy-mm-dd
                Form.validators.regexp
                    message: trans 'forms.errors.invalid_date_format'
                    regexp: /^\d{4}-\d{1,2}-\d{1,2}$/

            email: ->
                Form.validators.regexp
                    message: trans 'forms.errors.invalid_email'
                    regexp: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

        return
